'use strict';
var config = module.exports;
var pjson = require("../package.json");
var PRODUCTION = process.env.NODE_ENV === 'production';

config.express = {
    port: process.env.EXPRESS_PORT || 3000,
    ip: process.env.EXPRESS_HOST || '127.0.0.1'
};

// config.mongodb = {
//     port: process.env.MONGODB_PORT || 27017,
//     host: process.env.MONGODB_HOST || 'localhost'
// };
config.api = 'http://127.0.0.1:3001';
config.package = pjson;

if (PRODUCTION) {

}


(function(){
    'use strict';
    angular
        .module("navbarModule", ['ui.router','ngSanitize','CoreModule'])
        .config(configState);

    function configState($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');
        var mainState = {
            name: 'navbar',
            url: '/',
            templateUrl: '/client/_navbar/views/_navbar.html',
            cache: false
        };
        $stateProvider.state(mainState);
    }
})();

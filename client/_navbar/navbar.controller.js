/**
 * Created by phuon on 5/30/2017.
 */
(function() {
    'use strict';
    angular
        .module("navbarModule")
        .controller("NavbarController", navbarController);

    function navbarController($cookies) {
        var vm = this;
        vm.init = function () {

        }
        vm.checkUser = function () {
            vm.cookieUserID = $cookies.getObject('userID');
            if(vm.cookieUserID  == '' || vm.cookieUserID  == undefined){
                return false;
            }
            else {
                vm.username = $cookies.getObject('userName');
                return true;
            }
        }
    }
})();
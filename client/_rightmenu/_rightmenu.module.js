(function(){
    'use strict';
    angular
        .module("rightmenuModule", ['ui.router','ngSanitize'])
        .config(configState);

    function configState($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');
        var mainState = {
            name: 'rightmenu',
            url: '/',
            templateUrl: '/client/_rightmenu/views/_rightmenu.html',
            cache: false
        };
        $stateProvider.state(mainState);
    }
})();

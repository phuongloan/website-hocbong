/**
 * Created by phuon on 5/30/2017.
 */
(function() {
    'use strict';
    angular
        .module("rightmenuModule")
        .controller("RightMenuController", rightMenuController);

    function rightMenuController() {
        var vm = this;
        vm.init = function () {

        }
        vm.changeMode = function () {
            $('.search-detail').toggle();
            $('.search-keyword').toggle();
        }
    }
})();
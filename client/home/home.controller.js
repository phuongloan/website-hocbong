/**
 * Created by phuon on 5/13/2017.
 */
(function() {
    'use strict';
    angular
        .module("homeModule")
        .controller("homeController", homeController);

    function homeController($scope,$http, HomeService, $cookies) {
        var vm = this;
        vm.init = function () {
            console.log($cookies.getAll());
            console.log('data.data');
            HomeService.getAllHome().then(function (data) {
                console.log('data.data');
                console.log(data.data);
                vm.data = data.data;
            });
        }


    }
})();
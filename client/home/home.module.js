(function(){
    'use strict';
    angular
        .module("homeModule", ['ui.router','CoreModule'])
        .config(configState);

    function configState($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');
        var mainState = {
            name: 'home',
            url: '/',
            templateUrl: '/client/home/views/home.html',
            cache: false
        };
        $stateProvider.state(mainState);
    }
})();

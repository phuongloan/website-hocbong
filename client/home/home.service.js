/**
 * Created by phuon on 5/13/2017.
 */
(function(){
    'use strict';
    angular
        .module("homeModule")
        .factory("HomeService", homeService);

    function homeService(baseService){

        return {
            getAllHome: getAllHome
        };

        function getAllHome(){
           return baseService.getBase('home/getAll');
        }
    }
})();
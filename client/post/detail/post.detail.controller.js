/**
 * Created by phuon on 5/14/2017.
 */
(function() {
    'use strict';
    angular
        .module("postModule")
        .controller("postDetailController", postDetailController);

    function postDetailController(PostService) {
        var vm = this;
        vm.init = function () {
            var query = config.queryURL;
           PostService.getPostById(query).then(function (data) {
               console.log(data.data);
               vm.post = data.data[0];
               $('#noidung').append(vm.post.noidung);
           }) ;
            // getNoidungCkeditor();
        }
        // function getNoidungCkeditor() {

        // }
    }
})();
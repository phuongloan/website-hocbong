/**
 * Created by phuon on 5/14/2017.
 */
(function() {
    'use strict';
    angular
        .module("postModule")
        .controller("postController", postController);

    function postController($scope,$http, PostService,$filter, $cookies) {
        var vm = this;
        vm.post = {};
        vm.options = {
            language: 'en',
            allowedContent: true,
            entities: false
        };
        vm.init = function () {
            if($cookies.getObject('userID') == '' || $cookies.getObject('userID') == undefined){
                config.goto('/');
            }
            vm.post.idnguoidang = $cookies.getObject('userID');
            vm.post.tennguoidang = $cookies.getObject('userName');
            var today = $filter('date')(new Date(),'yyyy-MM-dd');
            vm.post.ngaydang = today;
        }
        vm.submit = function () {
            if(checkValidForm()){
                vm.post.handangki =  $filter('date')(vm.post.handangki,'yyyy-MM-dd');
                PostService.postBaiViet(vm.post).then(function (respon) {
                    console.log(respon);
                });
            }
        }
        vm.onReady = function () {
            // ...
        };
        function checkValidForm() {
            return true;
        }
        function getUserInfo() {
            UserService.getUserByID('id='+vm.post.idnguoidang).then(function (data) {
                vm.post.tennguoidang = data.data[0].username;
            })
        }
    }
})();
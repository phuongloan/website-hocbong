/**
 * Created by phuon on 4/22/2017.
 */
(function(){
    'use strict';
    angular
        .module("postModule", ['ui.router','ngSanitize','CoreModule', 'ngCookies','ckeditor'])
        .config(configState);

    function configState($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');
        var mainState = {
            name: 'post',
            url: '/',
            templateUrl: '/client/post/detail/views/postDetail.html',
            cache: false
        };
        $stateProvider.state(mainState);
    }
})();
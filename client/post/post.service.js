/**
 * Created by phuon on 5/14/2017.
 */
(function(){
    'use strict';
    angular
        .module("postModule")
        .factory("PostService", postService);

    function postService(baseService){

        return {
            postBaiViet: postBaiViet,
            getPostById : getPostById
        };

        function postBaiViet(data){
            return baseService.postBase('post/postBaiViet', data);
        }
        function getPostById(params) {
            return baseService.getBase('post/getPostById', params);
        }
    }
})();
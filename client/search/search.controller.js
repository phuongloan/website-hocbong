/**
 * Created by phuon on 5/14/2017.
 */
(function() {
    'use strict';
    angular
        .module("searchModule")
        .controller("SearchController", searchController);

    function searchController(SearchService) {
        var vm = this;
        vm.init = function () {
            var query = config.queryURL;
            SearchService.searchPost(query).then(function (result) {
                console.log(result.data);
                vm.results = result.data;
            }) ;
        }
    }
})();
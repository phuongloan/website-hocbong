/**
 * Created by phuon on 4/22/2017.
 */
(function(){
    'use strict';
    angular
        .module("searchModule", ['ui.router','ngSanitize','CoreModule'])
        .config(configState);

    function configState($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');
        var mainState = {
            name: 'search',
            url: '/',
            templateUrl: '/client/search/views/searchResults.html',
            cache: false
        };
        $stateProvider.state(mainState);
    }
})();
/**
 * Created by phuon on 5/14/2017.
 */
(function(){
    'use strict';
    angular
        .module("searchModule")
        .factory("SearchService", searchService);

    function searchService(baseService){

        return {
            searchPost : searchPost
        };

        function searchPost(query) {
            return baseService.getBase('search', query);
        }
    }
})();
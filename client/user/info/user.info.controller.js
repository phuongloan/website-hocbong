/**
 * Created by phuon on 5/14/2017.
 */
(function() {
    'use strict';
    angular
        .module("userModule")
        .controller("userInfoController", userInfoController);

    function userInfoController($scope,$http, UserService, $cookies) {
        var vm = this;
        vm.init = function () {
            console.log('cookies ', $cookies.getAll());
            // if($cookies.getObject('userID') == '' || $cookies.getObject('userID') == undefined){
            //     config.goto('/user/login');
            // }
            // else {
                var id = config.queryURL;
                UserService.getUserByID(id).then(function (data) {
                    vm.user = data.data[0];
                    console.log(vm.user);
                    UserService.getPostByIDUser('idnguoidang='+vm.user.id).then(function (result) {
                        console.log(result.data);
                        vm.user.post = result.data;
                    })
                });


            // }

        }
        vm.checkSection = function () {
            if($cookies.getObject('userID') == '' || $cookies.getObject('userID') == undefined){
                    return false;
                }
            else {
                if($cookies.getObject('userID') != vm.user.id){
                    return false;
                }
                return true;
            }
        }
        vm.logOut = function () {
            $cookies.remove('userID');
            $cookies.remove('userUsername');
            config.goto('/user/login');
        }


    }
})();
/**
 * Created by phuon on 5/14/2017.
 */
(function() {
    'use strict';
    angular
        .module("userModule")
        .controller("userLoginController", userLoginController);

    function userLoginController($scope,$http, UserService,$cookies) {
        var vm = this;

        vm.init = function () {
            // console.log(UserService.register(data));
        }
        vm.login = function () {
            UserService.login(vm.username, vm.password).then(function (data) {
                vm.result = data.data;
                console.log('result', vm.result);
                if(vm.result.status == 1){
                    //set cookies
                    var today = new Date();
                    var expiresValue = new Date(today);

                    //Set 'expires' option in 1 minute
                    expiresValue.setMinutes(today.getMinutes() + 5);

                    $cookies.putObject('userID',vm.result.user.id ,  {'expires' : expiresValue, path: '/'});
                    $cookies.putObject('userName',vm.result.user.username ,  {'expires' : expiresValue, path: '/'});
                    console.log('cookies ', vm.result.user.username);
                    //chuyen sang trang user detail
                    config.goto('/user?id='+vm.result.user.id);
                }
                else {
                    //o lai trang login
                    console.log(vm.result.massage);
                }
            });


        }
    }
})();
/**
 * Created by phuon on 5/14/2017.
 */
(function() {
    'use strict';
    angular
        .module("userModule")
        .controller("userRegisterController", userRegisterController);

    function userRegisterController($scope,$http, UserService,$cookies) {
        var vm = this;
        vm.init = function () {
            console.log($cookies.getObject('userID'));
        }
        vm.register = function () {
            UserService.register(vm.user).then(function (respon) {
                console.log(respon);
                if(respon.data == 'Ok'){
                    config.goto('/user/login');
                }
            })
        }
    }
})();
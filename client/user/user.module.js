/**
 * Created by phuon on 4/22/2017.
 */
(function(){
    'use strict';
    angular
        .module("userModule", ['ui.router','ngSanitize', 'CoreModule'])
        .config(configState);

    function configState($stateProvider, $urlRouterProvider){
        // $urlRouterProvider.otherwise('/');
        var mainState = {
            name: 'user',
            url: '/',
            templateUrl: '/client/user/info/views/info.html',
            cache: false
        };
        $stateProvider.state(mainState);
    }
})();
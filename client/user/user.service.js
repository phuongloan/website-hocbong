/**
 * Created by phuon on 5/14/2017.
 */
(function(){
    'use strict';
    angular
        .module("userModule")
        .factory("UserService", userService);

    function userService(baseService){

        return {
            register : register,
            login : login,
            getUserByID : getUserByID,
            getPostByIDUser: getPostByIDUser
        };

        function register(data){
            return baseService.postBase('user/register', data);
        }
        function login(username, password) {
            var data = {
                username: username,
                password: password
            }
            return baseService.postBase('user/login', data);

        }
        function getUserByID(id) {
            return baseService.getBase('user/getUserById', id);
        }
        function getPostByIDUser(userID) {
            return baseService.getBase('search', userID);
        }
    }
})();
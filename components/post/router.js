/**
 * Created by phuon on 4/22/2017.
 */
'use strict';
var router = require('express').Router();


function addPost(req, res) {
    res.render('addPost');
}
function postDetail(req, res) {
    console.log('---------------------------',req.query);
    res.render('postDetail');
}
router.get('/post/add', addPost);
router.get('/post', postDetail);

module.exports = router;


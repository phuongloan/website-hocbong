/**
 * Created by phuon on 4/22/2017.
 */
'use strict';
var router = require('express').Router();


function search(req, res) {
    res.render('searchResults');
}
router.get('/search', search);

module.exports = router;


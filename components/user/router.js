/**
 * Created by phuon on 4/22/2017.
 */
'use strict';
var router = require('express').Router();


function info(req, res) {
    res.render('info');
}
function login(req, res) {
    res.render('login');
}
function register(req, res) {
    res.render('register');
}
function logout(req, res) {
    res.render('logout');
}
router.get('/user', info);
router.get('/user/login', login);
router.get('/user/register', register);
router.get('/user/logout', logout);

module.exports = router;


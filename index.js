'use strict';
var express = require('express');
var engine = require('ejs-mate');
var helmet = require('helmet');
var connection = require('./database');
// var mysql = require('mysql');
// var connection = mysql.createConnection({
//     host     : 'localhost',
//     user     : 'root',
//     password : 'Loan2812',
//     database : 'website-hocbong'
// });
var app = express();
// var error = require('./components/_error');
var bodyParser  = require('body-parser');
// var lessExpress = require('less-express');

//Register view of each component
function registerViews (){
    var views = [];
    views.push(__dirname + "/components/_common/views");
    views.push(__dirname + "/components/home/views");
    views.push(__dirname + "/components/post/views");
    views.push(__dirname + "/components/search/views");
    // views.push(__dirname + "/components/portfolios/add.post/views");
    views.push(__dirname + "/components/user/views");
    // views.push(__dirname + "/components/assignment/views");
    return views;
}

app.engine('ejs', engine);
app.set('views',registerViews());

// Register view engine
app.set('view engine', 'ejs');

//Protect app
app.use(helmet());

//Register json parser
app.use(bodyParser.json());

//Register static assets
app.use('/assets', express.static('assets'));
app.use('/client',express.static('client'));



// // Enable cross ajax request
// app.all('/*', function(req, res, next) {
//     res.header('Access-Control-Allow-Origin', '*');
//     res.header('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With');
//     res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
//     next();
// });

// //test connect database
// connection.connect(function(err){
//     if(!err) {
//         console.log("Database is connected ... nn");
//     } else {
//         console.log("Error connecting database ... nn");
//     }
// });


// //register less style sheet
// app.get('/css/context-menu.css', lessExpress('./assets/css/component/context-menu.less'));

// Load the routes ("controllers" -ish)
app.use(require('./components/home/router'));
app.use(require('./components/post/router'));
app.use(require('./components/search/router'));
app.use(require('./components/user/router'));
// app.use(require('./components/portfolios/router'));
// app.use(require('./components/assignment/router'));
// Repeat the above line for additional model areas ("deals", "vehicles", etc)



// FINALLY, use any error handlers
// error.registerErrorHandler(app);

console.log(process.env.NODE_ENV);

// Export the app instance for unit testing via supertest
module.exports = app;




